\begin{thebibliography}{26}
\expandafter\ifx\csname natexlab\endcsname\relax\def\natexlab#1{#1}\fi

\bibitem[{Battaglia et~al.(2016)Battaglia, Pascanu, Lai, Rezende, and
  kavukcuoglu}]{interaction}
Peter Battaglia, Razvan Pascanu, Matthew Lai, Danilo~Jimenez Rezende, and Koray
  kavukcuoglu. 2016.
\newblock Interaction networks for learning about objects, relations and
  physics.
\newblock In \emph{Proceedings of the 30th International Conference on Neural
  Information Processing Systems}, NIPS'16, pages 4509--4517, USA. Curran
  Associates Inc.

\bibitem[{Battaglia et~al.(2018)Battaglia, Hamrick, Bapst, Sanchez-Gonzalez,
  Zambaldi, Malinowski, Tacchetti, Raposo, Santoro, Faulkner
  et~al.}]{graph_review}
Peter~W Battaglia, Jessica~B Hamrick, Victor Bapst, Alvaro Sanchez-Gonzalez,
  Vinicius Zambaldi, Mateusz Malinowski, Andrea Tacchetti, David Raposo, Adam
  Santoro, Ryan Faulkner, et~al. 2018.
\newblock Relational inductive biases, deep learning, and graph networks.
\newblock \emph{arXiv preprint arXiv:1806.01261}.

\bibitem[{Bollacker et~al.(2008)Bollacker, Evans, Paritosh, Sturge, and
  Taylor}]{freebase}
Kurt Bollacker, Colin Evans, Praveen Paritosh, Tim Sturge, and Jamie Taylor.
  2008.
\newblock \href {https://doi.org/10.1145/1376616.1376746} {Freebase: A
  collaboratively created graph database for structuring human knowledge}.
\newblock In \emph{Proceedings of the 2008 ACM SIGMOD International Conference
  on Management of Data}, SIGMOD '08, pages 1247--1250, New York, NY, USA. ACM.

\bibitem[{Bordes et~al.(2012)Bordes, Glorot, Weston, and Bengio}]{um}
Antoine Bordes, Xavier Glorot, Jason Weston, and Yoshua Bengio. 2012.
\newblock \href {http://proceedings.mlr.press/v22/bordes12.html} {Joint
  learning of words and meaning representations for open-text semantic
  parsing}.
\newblock In \emph{Proceedings of the Fifteenth International Conference on
  Artificial Intelligence and Statistics}, volume~22 of \emph{Proceedings of
  Machine Learning Research}, pages 127--135, La Palma, Canary Islands. PMLR.

\bibitem[{Bordes et~al.(2013)Bordes, Usunier, Garcia-Duran, Weston, and
  Yakhnenko}]{transe}
Antoine Bordes, Nicolas Usunier, Alberto Garcia-Duran, Jason Weston, and Oksana
  Yakhnenko. 2013.
\newblock \href
  {http://papers.nips.cc/paper/5071-translating-embeddings-for-modeling-multi-relational-data.pdf}
  {Translating embeddings for modeling multi-relational data}.
\newblock In C.~J.~C. Burges, L.~Bottou, M.~Welling, Z.~Ghahramani, and K.~Q.
  Weinberger, editors, \emph{Advances in Neural Information Processing Systems
  26}, pages 2787--2795. Curran Associates, Inc., Lake Tahoe, USA.

\bibitem[{Bordes et~al.(2011{\natexlab{a}})Bordes, Weston, Collobert, and
  Bengio}]{se}
Antoine Bordes, Jason Weston, Ronan Collobert, and Yoshua Bengio.
  2011{\natexlab{a}}.
\newblock \href {http://dl.acm.org/citation.cfm?id=2900423.2900470} {Learning
  structured embeddings of knowledge bases}.
\newblock In \emph{Proceedings of the Twenty-Fifth AAAI Conference on
  Artificial Intelligence}, AAAI'11, pages 301--306. AAAI Press.

\bibitem[{Bordes et~al.(2011{\natexlab{b}})Bordes, Weston, Collobert, and
  Bengio}]{linkprediction}
Antoine Bordes, Jason Weston, Ronan Collobert, and Yoshua Bengio.
  2011{\natexlab{b}}.
\newblock \href {http://dl.acm.org/citation.cfm?id=2900423.2900470} {Learning
  structured embeddings of knowledge bases}.
\newblock In \emph{Proceedings of the Twenty-Fifth AAAI Conference on
  Artificial Intelligence}, AAAI'11, pages 301--306, San Francisco, California.
  AAAI Press.

\bibitem[{Dettmers et~al.(2018)Dettmers, Pasquale, Pontus, and Riedel}]{conve}
Tim Dettmers, Minervini Pasquale, Stenetorp Pontus, and Sebastian Riedel. 2018.
\newblock \href {https://arxiv.org/abs/1707.01476} {Convolutional 2d knowledge
  graph embeddings}.
\newblock In \emph{Proceedings of the 32th AAAI Conference on Artificial
  Intelligence}, pages 1811--1818.

\bibitem[{Gilmer et~al.(2017)Gilmer, Schoenholz, Riley, Vinyals, and
  Dahl}]{mpnn}
Justin Gilmer, Samuel~S. Schoenholz, Patrick~F. Riley, Oriol Vinyals, and
  George~E. Dahl. 2017.
\newblock \href {http://proceedings.mlr.press/v70/gilmer17a.html} {Neural
  message passing for quantum chemistry}.
\newblock In \emph{Proceedings of the 34th International Conference on Machine
  Learning}, volume~70 of \emph{Proceedings of Machine Learning Research},
  pages 1263--1272, International Convention Centre, Sydney, Australia. PMLR.

\bibitem[{He et~al.(2015)He, Liu, Ji, and Zhao}]{kg2e}
Shizhu He, Kang Liu, Guoliang Ji, and Jun Zhao. 2015.
\newblock \href {https://doi.org/10.1145/2806416.2806502} {Learning to
  represent knowledge graphs with gaussian embedding}.
\newblock In \emph{Proceedings of the 24th ACM International on Conference on
  Information and Knowledge Management}, CIKM '15, pages 623--632, New York,
  NY, USA. ACM.

\bibitem[{Ji et~al.(2015)Ji, He, Xu, Liu, and Zhao}]{transd}
Guoliang Ji, Shizhu He, Liheng Xu, Kang Liu, and Jun Zhao. 2015.
\newblock Knowledge graph embedding via dynamic mapping matrix.
\newblock In \emph{{ACL} {(1)}}, pages 687--696, Beijing, China. The
  Association for Computer Linguistics.

\bibitem[{Ji et~al.(2016)Ji, Liu, He, and Zhao}]{transparse}
Guoliang Ji, Kang Liu, Shizhu He, and Jun Zhao. 2016.
\newblock \href {http://dl.acm.org/citation.cfm?id=3015812.3015959} {Knowledge
  graph completion with adaptive sparse transfer matrix}.
\newblock In \emph{Proceedings of the Thirtieth AAAI Conference on Artificial
  Intelligence}, AAAI'16, pages 985--991. AAAI Press.

\bibitem[{Li et~al.(2015)Li, Tarlow, Brockschmidt, and Zemel}]{ggnn}
Yujia Li, Daniel Tarlow, Marc Brockschmidt, and Richard Zemel. 2015.
\newblock Gated graph sequence neural networks.
\newblock \emph{arXiv preprint arXiv:1511.05493}.

\bibitem[{Lin et~al.(2015)Lin, Liu, Sun, Liu, and Zhu}]{transr}
Yankai Lin, Zhiyuan Liu, Maosong Sun, Yang Liu, and Xuan Zhu. 2015.
\newblock \href {http://dl.acm.org/citation.cfm?id=2886521.2886624} {Learning
  entity and relation embeddings for knowledge graph completion}.
\newblock In \emph{Proceedings of the Twenty-Ninth AAAI Conference on
  Artificial Intelligence}, AAAI'15, pages 2181--2187, Austin, Texas. AAAI
  Press.

\bibitem[{Miller(1995)}]{wordnet}
George~A Miller. 1995.
\newblock Wordnet: a lexical database for english.
\newblock \emph{Communications of the ACM}, 38(11):39--41.

\bibitem[{Nickel et~al.(2016)Nickel, Rosasco, and Poggio}]{hole}
Maximilian Nickel, Lorenzo Rosasco, and Tomaso Poggio. 2016.
\newblock \href {http://dl.acm.org/citation.cfm?id=3016100.3016172}
  {Holographic embeddings of knowledge graphs}.
\newblock In \emph{Proceedings of the Thirtieth AAAI Conference on Artificial
  Intelligence}, AAAI'16, pages 1955--1961, Phoenix, Arizona. AAAI Press.

\bibitem[{Nickel et~al.(2011)Nickel, Tresp, and Kriegel}]{rescal}
Maximilian Nickel, Volker Tresp, and Hans-Peter Kriegel. 2011.
\newblock \href {http://dl.acm.org/citation.cfm?id=3104482.3104584} {A
  three-way model for collective learning on multi-relational data}.
\newblock In \emph{Proceedings of the 28th International Conference on
  International Conference on Machine Learning}, ICML'11, pages 809--816,
  Bellevue, Washington, USA. Omnipress.

\bibitem[{Schlichtkrull et~al.(2017)Schlichtkrull, Kipf, Bloem, Berg, Titov,
  and Welling}]{rgcn}
Michael Schlichtkrull, Thomas~N Kipf, Peter Bloem, Rianne van~den Berg, Ivan
  Titov, and Max Welling. 2017.
\newblock Modeling relational data with graph convolutional networks.
\newblock \emph{arXiv preprint arXiv:1703.06103}.

\bibitem[{Sch{\"u}tt et~al.(2017)Sch{\"u}tt, Arbabzadah, Chmiela, M{\"u}ller,
  and Tkatchenko}]{deeptensor}
Kristof~T Sch{\"u}tt, Farhad Arbabzadah, Stefan Chmiela, Klaus~R M{\"u}ller,
  and Alexandre Tkatchenko. 2017.
\newblock Quantum-chemical insights from deep tensor neural networks.
\newblock \emph{Nature communications}, 8:13890.

\bibitem[{Socher et~al.(2013)Socher, Chen, Manning, and Ng}]{ntn}
Richard Socher, Danqi Chen, Christopher~D Manning, and Andrew Ng. 2013.
\newblock \href
  {http://papers.nips.cc/paper/5028-reasoning-with-neural-tensor-networks-for-knowledge-base-completion.pdf}
  {Reasoning with neural tensor networks for knowledge base completion}.
\newblock In C.~J.~C. Burges, L.~Bottou, M.~Welling, Z.~Ghahramani, and K.~Q.
  Weinberger, editors, \emph{Advances in Neural Information Processing Systems
  26}, pages 926--934. Curran Associates, Inc., Lake Tahoe, USA.

\bibitem[{Toutanova and Chen(2015)}]{leakage}
Kristina Toutanova and Danqi Chen. 2015.
\newblock \href {https://doi.org/10.18653/v1/W15-4007} {Observed versus latent
  features for knowledge base and text inference}.
\newblock In \emph{Proceedings of the 3rd Workshop on Continuous Vector Space
  Models and their Compositionality}, pages 57--66. Association for
  Computational Linguistics.

\bibitem[{Trouillon et~al.(2016)Trouillon, Welbl, Riedel, Gaussier, and
  Bouchard}]{complex}
Th{\'e}o Trouillon, Johannes Welbl, Sebastian Riedel, \'{E}ric Gaussier, and
  Guillaume Bouchard. 2016.
\newblock \href {http://dl.acm.org/citation.cfm?id=3045390.3045609} {Complex
  embeddings for simple link prediction}.
\newblock In \emph{Proceedings of the 33rd International Conference on
  International Conference on Machine Learning - Volume 48}, ICML'16, pages
  2071--2080. JMLR.org.

\bibitem[{Veli{\v{c}}kovi{\'{c}} et~al.(2018)Veli{\v{c}}kovi{\'{c}}, Cucurull,
  Casanova, Romero, Li{\`{o}}, and Bengio}]{gat}
Petar Veli{\v{c}}kovi{\'{c}}, Guillem Cucurull, Arantxa Casanova, Adriana
  Romero, Pietro Li{\`{o}}, and Yoshua Bengio. 2018.
\newblock \href {https://openreview.net/forum?id=rJXMpikCZ} {{Graph Attention
  Networks}}.
\newblock \emph{International Conference on Learning Representations}.
\newblock Accepted as poster.

\bibitem[{Wang et~al.(2017)Wang, Mao, Wang, and Guo}]{embedding_review}
Quan Wang, Zhendong Mao, Bin Wang, and Li~Guo. 2017.
\newblock \href
  {http://dblp.uni-trier.de/db/journals/tkde/tkde29.html#WangMWG17} {Knowledge
  graph embedding: A survey of approaches and applications.}
\newblock \emph{IEEE Trans. Knowl. Data Eng.}, 29(12):2724--2743.

\bibitem[{Wang et~al.(2014)Wang, Zhang, Feng, and Chen}]{transh}
Zhen Wang, Jianwen Zhang, Jianlin Feng, and Zheng Chen. 2014.
\newblock \href {http://dl.acm.org/citation.cfm?id=2893873.2894046} {Knowledge
  graph embedding by translating on hyperplanes}.
\newblock In \emph{Proceedings of the Twenty-Eighth AAAI Conference on
  Artificial Intelligence}, AAAI'14, pages 1112--1119, Qu\&\#233;bec City,
  Qu\&\#233;bec, Canada. AAAI Press.

\bibitem[{Yang et~al.(2014)Yang, Yih, He, Gao, and Deng}]{distmult}
Bishan Yang, Wen-tau Yih, Xiaodong He, Jianfeng Gao, and Li~Deng. 2014.
\newblock Embedding entities and relations for learning and inference in
  knowledge bases.
\newblock \emph{arXiv preprint arXiv:1412.6575}.

\end{thebibliography}
